import React from 'react';
import { useStaticQuery, graphql, Link } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image'; // Changed import for GatsbyImage
const HeaderLogo = () => {
    const data = useStaticQuery(graphql`
        query MyQuery {
            file(relativePath: { eq: "logo.png" }) {
                childImageSharp {
                    gatsbyImageData(layout: FULL_WIDTH)
                  }
            }
        }
    `);
    return (
        <div>
            <Link to='/'>
    
                <GatsbyImage
                                    className='header-logo'
                                    image={getImage(data.file)}
                                    alt='FOSS NSS Logo'
                                    // Added alt attribute for accessibility
                                />
            </Link>
        </div>
    );
};
export default HeaderLogo;