---
name: "Siv Hari Nair"
avathar: "https://avatars.githubusercontent.com/u/68660329?v=4"
designation: "Technical Coordinator"
url: "https://github.com/sivharinair2001/"
dept: "CSE"
email: "nairsivhari@gmail.com"
phone: "+91 859 066 7564"
skills: "Web Developer & Ethical Hacker"
---