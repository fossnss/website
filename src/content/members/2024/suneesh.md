---
name: "Suneesh S"
avathar: "https://avatars.githubusercontent.com/u/100022002"
designation: "Technical Coordinator"
url: "https://github.com/kalminhere"
dept: "ME"
email: "suneesh@duck.com"
phone: "+91 9188242412"
skills: "Android Developer"
---